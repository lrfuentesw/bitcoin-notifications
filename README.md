# Bitcoin Notifications

A bitcoin price notification in python. This code was writen following [this tutorial](https://realpython.com/python-bitcoin-ifttt/). This version gets bitcoin price from [coinbase api](https://developers.coinbase.com/api/v2?python#get-sell-price) and uses [IFTTT](https://ifttt.com/) for notifications.

This version gets ifttt key from environ variable `IFTTT_KEY`.