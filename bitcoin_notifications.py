"""A bitcoin price notification
"""
import os

import time
from datetime import datetime
import requests

IFFTT_KEY = os.getenv("IFTTT_KEY")

BITCOIN_API_URL = 'https://api.coinbase.com/v2/prices/{}/spot'
IFTTT_WEBHOOKS_URL = ('https://maker.ifttt.com/trigger/{}/with/key/' +
                      str(IFFTT_KEY))
BITCOIN_PRICE_MAX = 10000
BITCOIN_PRICE_MIN = 8500


def get_latest_bitcoin_price(currency_pair):
    """Get bitcoin price from coinbase

    :param currency_pair: Currency pair. Example: BTC-USD
    :type currency_pair: str
    :return: Bitcoin price
    :rtype: float
    """
    response = requests.get(BITCOIN_API_URL.format(currency_pair))
    response_json = response.json()
    # Convert the price to a floating point number
    return float(response_json['data']['amount'])


def post_ifttt_webhook(event, value):
    """Send a post request to IFTTT webhook

    :param event: Event name
    :type event: str
    :param value: Data to send
    :type value: str
    """
    # The payload that will be sent to IFTTT service
    data = {'value1': value}
    # inserts our desired event
    ifttt_event_url = IFTTT_WEBHOOKS_URL.format(event)
    # Sends a HTTP POST request to the webhook URL
    requests.post(ifttt_event_url, json=data)


def format_bitcoin_history(bitcoin_history):
    """Gives format to bitcoin history

    :param bitcoin_history: Bitcoin prices
    :type bitcoin_history: list
    :return: Formated bitcoin price history
    :rtype: str
    """
    rows = []
    for bitcoin_price in bitcoin_history:
        # Formats the date into a string: '2018-02-24 15:09'
        date = bitcoin_price['date'].strftime('%Y-%m-%d %H:%M')
        price = bitcoin_price['price']
        perc_change = bitcoin_price['perc_change']
        # <b> (bold) tag creates bolded text
        # 24.02.2018 15:09: $<b>10123.4</b>
        row = '{}: $<b>{}</b> {}%'.format(date, price, perc_change)
        rows.append(row)

    # Use a <br> (break) tag to create a new line
    # Join the rows delimited by <br> tag: row1<br>row2<br>row3
    return '<br>'.join(rows)


def percentage_change(price_0, price_1):
    """Calculates percentage change from last bitcoin price.
    :param price_0: Last bitcoin price
    :type price_0: float
    :param price_1: New bitcoin price
    :type price_1: float
    :return: Percentage change
    :rtype: float
    """
    result = (price_1 - price_0) / price_0
    return result


def main():
    """Main function
    """
    bitcoin_history = []
    while True:
        print("Consultando precio")
        price = get_latest_bitcoin_price('BTC-USD')
        date = datetime.now()

        if len(bitcoin_history) > 1:
            perc_change = percentage_change(bitcoin_history[-1]['price'],
                                            price)
        else:
            perc_change = 0.0000

        perc_change = round(perc_change, 4)
        print(f"Precio de bitcoin: {price} al {date}." +
              f" Percentage change {perc_change}")
        bitcoin_history.append({'date': date,
                                'price': price,
                                'perc_change': perc_change})

        # Emergency price notification
        if price < BITCOIN_PRICE_MIN or price > BITCOIN_PRICE_MAX:
            print("Consumiendo webhook")
            post_ifttt_webhook('bitcoin_price_emergency', price)

        # Telegram message with latest 10 bitcoin prices
        if len(bitcoin_history) > 9:
            print("Consumiendo webhook")
            post_ifttt_webhook('bitcoin_price_update',
                               format_bitcoin_history(bitcoin_history))
            # Reset bitcoin history
            bitcoin_history = []

        # Wait 1 min between each price update
        time.sleep(60)


if __name__ == '__main__':
    main()
